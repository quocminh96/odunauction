class UserMailer < ApplicationMailer
  def buynow(order)
    @order = order
    mail to: order.user.email, subject: "Buy product successful"
  end

  def contact(seller, sender, message, product)
  	@seller = seller
  	@sender = sender
  	@message = message
  	@product = product
  	mail to: seller.email, subject: "Buyer contact"
  end

  def win_auction(winner, product)
    @winner = winner
    @product = product
    mail to: winner.email, subject: "You won an auction"
  end

  def lose_auction(loser, product)
    @loser = loser
    @product = product
    mail to: loser.email, subject: "You lost an auction"
  end
end
