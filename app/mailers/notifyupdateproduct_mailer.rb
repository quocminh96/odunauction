class NotifyupdateproductMailer < ApplicationMailer
	layout "mailer"
	def sample_email(user)
	    @user = user
	    mail(to: @user.email, subject: 'Update notification', body: 'You has recently changed the product information');
 	 end
end