class AutoBidsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def create
    begin
      @product = Product.find(params[:product_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: ["Not Found"] }, status: 404
    else
      @auto_bid = AutoBid.create_and_get(@product, current_user)
      @current_max_bid = Bid.find_max_bid(@product.id)      
      if params[:max_bid_amount] != ''
        if higher_amount?
          @auto_bid.max_bid_amount = params[:max_bid_amount]  
          if @auto_bid.save
            @auto_bid.handle_after_created
            render json: @auto_bid, status: 200
          else
            render json: {errors: @auto_bid.errors.full_messages}, status: 403
          end
        else
          render json: { errors: ["Max bid amount must be higher than previous one"]}, status: 403
        end
      else
        render json: { errors: ["Max bid amount can't be blank"]}, status: 403
      end
    end
  end

  def higher_amount?
    step = 2
    max_bid = params[:max_bid_amount].to_f
    if @auto_bid.max_bid_amount == nil
      return true
    end
    if @auto_bid.max_bid_amount < max_bid && @current_max_bid.amount + step <= max_bid 
      return true
    end
    return false 
  end
end
