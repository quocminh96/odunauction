class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:contact]
  def show
    @user = User.find(params[:id])
  end

  def profile
    @user = current_user
    render 'show'
  end

  def wishlist
    @user = current_user
    @wishlists = current_user.wishlists
    render 'wishlist'
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(edit_user_params)
      redirect_to edit_user_path @user
    else
      redirect_to edit_user_path @user, alert: @user.errors.full_messages      
    end
  end

  def contact
    user = User.find_by(id: params[:id])
    message = params[:question]
    b = request.url.split("/")
    product = Product.find(b[b.length - 2])
    UserMailer.contact(user, current_user, message, product).deliver
    head 200
  end

  private
    def edit_user_params
      params.require(:user).permit(:username, :full_name,
                                    :street_addr, :city,
                                    :district, :zip_code,
                                    :country, :phone)
    end
end
