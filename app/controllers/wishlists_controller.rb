class WishlistsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def show
    @user = current_user
    @wishlist = @user.wishlists
  end

  def create
    @wishlist = Wishlist.new
		@wishlist.user = current_user
		@wishlist.product = Product.find(params[:p_id])
		if @wishlist.save
      render 'create'
		else
			render json: { error: @wishlist.errors.full_messages }, status: 403
		end
  end

  def destroy
		@product = Product.find(params[:p_id])
		@wishlist = Wishlist.find_by(product_id: @product.id, user_id: current_user.id)
		@wishlist.destroy
	end
end
