class ProductsController < ApplicationController
	skip_before_action :verify_authenticity_token
	def index
		if params[:name]
			@params = params[:name]
			@products = Product.where('name LIKE ?', "%#{params[:name]}%")
			render 'search'
		else
			@buynows = Product.where(has_buy_now: true)
			@auctions = Product.where(has_auction: true)
			@products = Product.all
			@user = current_user
			render 'index'
		end
	end

	def detail
		@product = Product.find(params[:id])
		@wishlist = Wishlist.find_by(product_id: @product.id, user_id: current_user)
		@bids = @product.bids.includes(:buyer).order(amount: :desc)
		@current_bid = @bids.first
		@products = Product.where.not(id: @product.id).where(cateid: @product.cateid).shuffle[0..3]
	end

	def new
		@product = Product.new
		@currentuser = current_user.id
		@category = Category.all
	end

	def show
    @product  = Product.find(params[:id])
    @pictures = @product.pictures
    @category= Category.find(@product.cateid)
    @seller = @product.seller

    respond_to do |format|
      format.html
      format.json { render json: @product }
    end
  end

	def edit
		@product = Product.find(params[:id])
		@currentuser=current_user.id
		@category = Category.all

	end


	def create
		@product = Product.new(product_params)
		@product.seller = current_user
		if @product.save
			if params[:images]

		        params[:images].each { |image|
		          @product.pictures.create(image: image)
		        }
      		end
			redirect_to products_path
		else
			redirect_to new_product_path, alert: @product.errors.full_messages
		end
	end

	def update
		@product = Product.find(params[:id])
    	if @product.update_attributes(product_params)
	    	if params[:images]
	        params[:images].each { |image|
	          @product.pictures.create(image: image)
	        }
	      end
	    NotifyupdateproductMailer.sample_email(current_user).deliver
	    redirect_to products_path
	   else
	      redirect_to edit_product_path, alert: @product.errors.full_messages
	   end
	end

	def destroy
	  @product = Product.find(params[:id])
	  @product.destroy

	  respond_to do |format|
	    format.html { redirect_to root_path }
	    format.js
    	end
	end

	def buynow
		@order = Order.new
		@order.user = current_user
		@order.product = Product.find(params[:id])
		@order.amount = params[:amount]
		@order.is_paid = false
		if @order.save
			head 201
			UserMailer.buynow(@order).deliver
		else
			render json: { error: @order.errors.full_messages }, status: 403
		end
	end

	private
		def product_params
			params.require(:product).permit(:name, :condition,:has_auction,:has_buy_now,
			:pictures,:details,:price_for_buy,:date_end_auction,:description,:auction_price_start,:cateid)
		end
end
