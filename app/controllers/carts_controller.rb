class CartsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def address
  end

  def delivery
  end

  def payment
  end

  def review
  end

  def confirm
  end

  def success
    if (params[:response][:payer][:status] == "VERIFIED")
      @orders = Order.get_unpaid(current_user)
      @orders.each do |order|
        order.update_attribute(:is_paid, true)
      end
    end
  end
end
