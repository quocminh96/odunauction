class BidsController < ApplicationController
  skip_before_action :verify_authenticity_token
  def create
    begin
      @product = Product.find(params[:product_id])
    rescue ActiveRecord::RecordNotFound => e
      redirect_to "/404" and return
    else
      @bid = Bid.new( product: @product, 
                      amount: params[:amount], 
                      buyer_id: current_user.id )
      if params[:amount] != ''
        if @bid.valid?
          head 201
        else
          render json: { errors: @bid.errors.full_messages }, status: 403
        end
      else
        render json: { errors: ["Bid amount can't be blank"] }, status: 403
      end        
    end
  end

  def index
    begin
      @product = Product.find(params[:product_id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: ["Not Found"] }, status: 404
    else
      @bids = @product.bids.order(amount: :desc)
      respond_to do |format|
        format.js
        format.json do
          render json: @bids, status: 200          
        end
      end
    end
  end

  def show
    begin
      @bid = Bid.find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: ["Not Found"] }, status: 404
    else
      render json: @bid, status: 200
    end
  end
end
