class CategoriesController < ApplicationController
  def show
    @category = Category.where('name LIKE ?', params[:id]).first
    @products = Product.where(cateid: @category.id)
  end
end