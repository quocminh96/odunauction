class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :get_main_info

  def get_main_info
  	@orders = Order.get_unpaid(current_user)
    @total = @orders.sum(:amount).round(2)
    @categories = Category.all
  end
end
