class BidChannel < ApplicationCable::Channel
  def subscribed
    stream_from "products-#{params[:product_id]}"
  end

  def unsubscribed
    stop_all_streams
  end

  def bid(data)
    begin
      @product = Product.find(data["product_id"])
    rescue ActiveRecord::RecordNotFound => e
      p "errors not found"
    else
      @bid = Bid.new( product: @product, 
                      amount: data["amount"], 
                      buyer_id: current_user.id )
      if data["amount"] != ''
        if @bid.save
          p @bid
        else
          p "errors"
          # render json: { errors: @bid.errors.full_messages }, status: 403          
        end
      else
        p "errors"
        # render json: { errors: ["Bid amount can't be blank"] }, status: 403
      end        
    end
  end
end
