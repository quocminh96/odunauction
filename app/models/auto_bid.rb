class AutoBid < ApplicationRecord
  belongs_to :product
  belongs_to :buyer, class_name: "User", foreign_key: "buyer_id"
  validates_uniqueness_of :product_id, scope: [:buyer_id]
  validates :max_bid_amount, numericality: { greater_than: 0 }, allow_nil: true

  def handle_after_created
    step = 2
    current_max_bid = Bid.find_max_bid(product_id)
    if current_max_bid
      if current_max_bid.buyer_id != buyer_id 
        Bid.create( product_id: product_id,
                    amount: current_max_bid.amount + step,
                    buyer_id: buyer_id )
      end
    else
      Bid.create( product_id: product_id,
                  amount: product.auction_price_start + step,
                  buyer_id: buyer_id )
    end
  end

  def self.create_and_get(product, user)
    auto_bids = where(product_id: product.id, buyer_id: user.id)
    if auto_bids.length == 1
      auto_bids.first
    else
      auto_bids = create(product_id: product.id, buyer_id: user.id)
    end
  end

  def self.get_product_auto_bids(bid)
    where("product_id = ? and max_bid_amount > ?", bid.product.id, bid.amount)
    .order(max_bid_amount: :desc)
  end

  def self.auto_place_bid(current_bid)
    step = 2
    auto_bids = get_product_auto_bids(current_bid)
    if auto_bids.length > 1
      max = auto_bids.first
      second_max = auto_bids.second
      if max.max_bid_amount - second_max.max_bid_amount > step
        Bid.create( product_id: max.product_id,
                    amount: second_max.max_bid_amount + step,
                    buyer_id: max.buyer_id )
      else
        Bid.create( product_id: max.product_id,
                    amount: max.max_bid_amount,
                    buyer_id: max.buyer_id )
      end
    elsif auto_bids.length == 1
      max = auto_bids.first
      if max.buyer_id != current_bid.buyer_id
        if max.max_bid_amount - current_bid.amount > step
          Bid.create( product_id: max.product_id,
                      amount: current_bid.amount + step,
                      buyer_id: max.buyer_id )
        else
          Bid.create( product_id: max.product_id,
                      amount: max.max_bid_amount,
                      buyer_id: max.buyer_id )
        end
      end
    end
  end
end
