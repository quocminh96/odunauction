class Order < ApplicationRecord
  belongs_to :user
  belongs_to :product

  def self.get_unpaid(user)
    where(is_paid: false, user: user)
  end

  def self.get_paid(user)
    where(is_paid: true, user: user)
  end
end
