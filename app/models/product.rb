class Product < ApplicationRecord
  has_many :pictures, :dependent => :destroy
  validates :name, presence: true
  validates :auction_price_start, presence: true

  validates :auction_price_start, numericality: { greater_than_or_equal: 0 }, :allow_nil => true
  validates :price_for_buy, numericality: { greater_than_or_equal: 0 }, :allow_nil => true

  has_many :bids, dependent: :destroy
  has_many :auto_bids, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :wishlists, dependent: :destroy
  after_create :trigger_bidding_countdown
  belongs_to :seller, class_name: "User", foreign_key: "seller_id"
  before_save :parse_timezone

  def parse_timezone
    if date_end_auction
      self.date_end_auction = date_end_auction - 7.hours
    end
  end
  
  def trigger_bidding_countdown
    if has_auction
      CheckWinnerJob.set(wait_until: date_end_auction).perform_later self
    end
  end
end
