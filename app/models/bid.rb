class Bid < ApplicationRecord
  belongs_to :product
  belongs_to :buyer, class_name: "User", foreign_key: "buyer_id"
  validate :can_bid
  validate :highest_bid
  after_create_commit :broadcast
  after_create :check_auto_bid

  def self.find_max_bid(product_id)
    bids = self.where(product_id: product_id).order(amount: :desc)
    bids.first if bids.length > 0
  end

  def can_bid
    @product = product
    unless @product.has_auction
      errors.add(:product, 'did not have auction')
    else
      if !@product.date_end_auction || @product.date_end_auction < Time.now.utc
        errors.add(:product, "'s auction had been ended")
      else
        if @product.auction_price_start > amount
          errors.add(:amount, "must be higher than #{@product.auction_price_start}")
        end
      end
    end
  end

  def highest_bid
    max_bid = Bid.find_max_bid(product_id)
    if max_bid && max_bid.amount >= amount
      errors.add(:amount, 'must be the highest')
    end
  end

  def broadcast
    BidBroadcastJob.perform_later(bid: self)    
  end

  def check_auto_bid
    AutoBid.auto_place_bid self
  end
end