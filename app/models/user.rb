class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :bids, class_name: "Bid", foreign_key: "buyer_id"
  has_many :orders, dependent: :destroy
  has_many :wishlists, dependent: :destroy
  has_many :products, foreign_key: "seller_id", dependent: :destroy
  
  before_create :create_username

  private
    def create_username
      self.username = self.email.split("@")[0]
    end
end
