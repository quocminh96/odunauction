class Picture < ApplicationRecord
	 belongs_to :product

  has_attached_file :image,
    # :path => ":rails_root/public/images/:id/:filename",
    # :url  => "/images/:id/:filename"
    :storage => :cloudinary,
     :path => ':id/:filename',
     :cloudinary_credentials => Rails.root.join("config/cloudinary.yml")

  do_not_validate_attachment_file_type :image
end
