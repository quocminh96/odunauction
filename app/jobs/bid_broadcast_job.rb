class BidBroadcastJob < ApplicationJob
  queue_as :default

  def perform(bid: nil)
    ActionCable.server.broadcast(
      "products-#{bid.product_id}",
      bid: bid
    )
  end
end
