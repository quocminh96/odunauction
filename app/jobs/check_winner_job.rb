class CheckWinnerJob < ApplicationJob
  queue_as :default

  def perform(product)
    bids = product.bids.order(amount: :desc)
    if bids.length > 0
      bid = bids.first
      p "Winner is #{bid.buyer.full_name} with bid amount $#{bid.amount}"
      Order.create(user: bid.buyer, amount: bid.amount, product: product)
      UserMailer.win_auction(bid.buyer, product).deliver
      losers_id = bids.where.not(buyer_id: bid.buyer.id)
              .pluck(:buyer_id).uniq
      losers = User.where(id: losers_id)
      losers.each{ |loser| UserMailer.lose_auction(loser, product).deliver }
    end
  end
end
