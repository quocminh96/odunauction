$(document).on('turbolinks:load', function(){
  var buy_url = window.location.pathname.split('/');
  var p_id = buy_url[2];
  $('#submit-buy').click(function(e) {
    var value = $('#submit-buy').text().split('$')[1];
    $.post('/products/'+p_id+'/buynow', {
      amount: value
    }).done(function(data) {
       $(e.target).addClass("disabled")
        $(e.target).parent().parent()
        .append("<div class='col text-success'>Buy successfully.</div>")
    }).fail(function(err) {
      $(e.target).parent().parent()
        .html("<div class='col text-danger'>An error has occur.</div>") 
    })
  })

  $('#submit-wish').click(e => {
    $.ajax({
      type: 'POST',
      url: '/wishlists',
      data: {
        p_id: p_id
      },
      success: function(result) {
      }
    });
  })

  $('#submit-unwish').click(e => {
    $.ajax({
      url: '/wishlists/destroy',
      type: 'DELETE',
      data: {
        p_id: p_id
      },
      success: function(result) {
        window.location.href = window.location.href
      }
    });
  })
})
