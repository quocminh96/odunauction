$(document).on('turbolinks:load',function() {
  var current_url = window.location.pathname.split('/');
  var id = current_url[2];
  if(current_url.includes("products") && current_url.includes("detail")) {
    App.bid = App.cable.subscriptions.create({channel: "BidChannel", product_id: id}, {
      connected: function() {
        // Called when the subscription is ready for use on the server
      },
    
      disconnected: function() {
        // Called when the subscription has been terminated by the server
      },
    
      received: function(data) {
        $('#current-bid-amount').html(data.bid.amount)
        $('input[name=bid-amount]').val(null);
        updateBidHistory();
      },
    
      bid: function(amount, product_id) {
        return this.perform('bid', {
          amount: amount,
          product_id: product_id
        });
      }
    });
  }
  $('#submit-bid').click(function(e) {
    var current_url = window.location.pathname.split('/');
    var id = current_url[2];
    var value = $('input[name=bid-amount]').val();   
    $.post('/products/'+id+'/bids', {
      amount: $('input[name=bid-amount]').val()
    }).done(function(data) {
      App.bid.bid(value, id);
    }).fail(function(err) {
      var errors = err.responseJSON.errors[0];
      var div = $('input[name=bid-amount]').parent().parent().parent();
      div.find('p[name=bid-errors]').append(errors + '<br/>');
      div.find('p[name=bid-errors]').removeClass("hidden");
    })
  })
})

function updateBidHistory(){
  var current_url = window.location.pathname.split('/');
  var id = current_url[2];
  $.ajax({
    url: '/products/'+id+'/bids',
    dataType: "script"
  })
}