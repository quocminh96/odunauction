$(document).on('turbolinks:load', function(){
  $("#auction").click(function() {
    $('html, body').animate({
        scrollTop: $("#auction-section").offset().top
    }, 2000);
  });

  $("#buynow").click(function() {
    $('html, body').animate({
        scrollTop: $("#buynow-section").offset().top
    }, 2000);
  });
})