$(document).on('turbolinks:load', function(){
  $('#submit-auto-bid').click(function(e) {
    var current_url = window.location.pathname.split('/');
    current_url.pop()
    current_url = current_url.join('/') + '/auto_bids'
    $.post(current_url, {
      max_bid_amount: $('input[name=auto-bid-amount]').val()
    }).done(function(data) {
      window.location.href = window.location.href;
    }).fail(function(err) {
      var errors = err.responseJSON.errors[0];
      var div = $('input[name=auto-bid-amount]').parent().parent().parent();
      div.find('p[name=bid-errors]').append(errors + '<br/>');
      div.find('p[name=bid-errors]').removeClass("hidden");
    })
  })
})