class AddFieldsToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :username, :string, unique: true
    add_column :users, :full_name, :string
    add_column :users, :street_addr, :string
    add_column :users, :city, :string
    add_column :users, :district, :string
    add_column :users, :zip_code, :integer
    add_column :users, :phone, :string
    add_column :users, :country, :string, :default => "Vietnam"
    add_column :users, :is_valid, :boolean, :default => false
  end
end
