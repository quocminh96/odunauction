class ChangeFieldsInProducts < ActiveRecord::Migration[5.1]
  def change
  	 change_column_default :products, :auction_price_start, 0
  	 change_column_default :products, :date_in,  Time.now
  	 change_column :products, :details, :string
  	 change_column_default :products, :price_for_buy, 0
 		 change_column_default :products, :status,  1
  end
end
