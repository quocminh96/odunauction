class ChangeColumnSellerOfUser < ActiveRecord::Migration[5.1]
  def change
    rename_column :products, :seller, :seller_id
  end
end
