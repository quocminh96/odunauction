class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.integer :condition
      t.integer :has_auction
      t.float :auction_price_start
      t.datetime :date_in
      t.string :details
      t.integer :has_buy_now
      t.float :price_for_buy
      t.datetime :date_end_auction
      t.integer :seller
      t.integer :status
      t.timestamps
    end
  end
end
