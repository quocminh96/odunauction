class CreateAutoBids < ActiveRecord::Migration[5.1]
  def change
    create_table :auto_bids do |t|
      t.bigint :buyer_id
      t.references :product, foreign_key: true
      t.float :max_bid_amount

      t.timestamps
    end
    
    add_index :auto_bids, [:buyer_id, :product_id], unique: true
  end
end
