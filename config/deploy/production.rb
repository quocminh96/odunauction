set :stage, :production
set :rails_env, :production
set :deploy_to, "deploy/apps/odunauction"
set :branch, :master
server '34.229.229.222', user: 'deploy', roles: %w(web app db), primary: true