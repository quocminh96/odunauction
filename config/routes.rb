Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  root to: "products#index"
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }


  resources :pictures
  resources :categories, only: [:show]
  resources :products do
    resources :bids, only: [:create, :index]
    resources :auto_bids, only: [:create]
    member do
      get 'detail'
      post 'buynow', to: 'products#buynow'
    end
  end

  resources :bids, only: [:show]

  resources :wishlists, only: [:show, :create] do
    collection do
      delete 'destroy'
    end
  end

  resources :users do
    collection do
      get 'profile'
      get 'wishlist'
    end
    member do
      post 'contact'
    end
  end


  resources :orders, only: [:index]

  resources :carts do
    collection do
      get 'address'
      get 'delivery'
      get 'review'
      get 'confirm'
      post 'success'
    end
  end

end
